
#include "PositivePowerUps.h"


PositivePowerUps::PositivePowerUps()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void PositivePowerUps::Update(const GameTime* pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	PowerUp::Update(pGameTime);
}


void PositivePowerUps::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	PowerUp::Initialize();
}


void PositivePowerUps::Hit(const float damage)
{
	PowerUp::Hit(damage);
}