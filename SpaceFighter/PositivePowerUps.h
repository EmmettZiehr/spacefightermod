#pragma once

#include "PowerUp.h"

class PositivePowerUps : public PowerUp
{

public:

	PositivePowerUps();
	virtual ~PositivePowerUps() { }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch) = 0;

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	virtual void Fire() { }

	virtual void Hit(const float damage);

	virtual std::string ToString() const { return "Positive Power Up"; }

	virtual CollisionType GetCollisionType() const { return CollisionType::POWERUP; }


protected:

	virtual double GetDelaySeconds() const { return m_delaySeconds; }


private:

	double m_delaySeconds;

	double m_activationSeconds;


};
