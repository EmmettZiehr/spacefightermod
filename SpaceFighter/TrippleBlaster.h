
#pragma once

#include "Weapon.h"

class TrippleBlaster : public Weapon
{

public:

	TrippleBlaster();

	TrippleBlaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.06; // Cooldown for each shot in burst
		m_longCooldown = 0.4; // Cooldown between bursts
		m_shotsCount = 0; // Shot count
		m_burstShots = 3; // Amount of shots in the burst

		m_activeSeconds = 15.0f;
	}

	virtual ~TrippleBlaster() { }

	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
		if (m_activeSeconds > 0)
		{
			m_activeSeconds -= pGameTime->GetTimeElapsed();
			if (m_activeSeconds <= 0) { Dectivate(); }
		}
	}

	virtual float GetActiveSeconds() { return m_activeSeconds; }

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void SetBurstShots(const int shots) { m_burstShots = shots; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);

					pProjectile->SetTextureIndex(1);

					// Incriments shots fired
					m_shotsCount++;

					// If shots fired is divisible by the burst shot amount then it has fired all shots
					// Otherwise it still needs to finish the burst
					// Compared to a variable so that it can be changed through power up
					if (m_shotsCount % m_burstShots == 0)
						m_cooldown = m_longCooldown;
					else 
						m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}

private:

	float m_cooldown;
	float m_cooldownSeconds;
	float m_longCooldown;
	int m_shotsCount;
	int m_burstShots;
	float m_activeSeconds;
};