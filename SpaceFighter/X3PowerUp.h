#pragma once


#include "PositivePowerUps.h"
#include "TrippleBlaster.h"
#include "Blaster.h"

class X3PowerUp : public PositivePowerUps
{

public:

	X3PowerUp();
	virtual ~X3PowerUp() { }

	static void SetTexture(Texture *pTexture) { s_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

private:

	static Texture *s_pTexture;

};