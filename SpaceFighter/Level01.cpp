

#include "Level01.h"
#include "BioEnemyShip.h"
#include "X3PowerUp.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	Texture *pTextureTwo = pResourceManager->Load<Texture>("Textures\\x3PowerUp.png"); //<- Texture set into a variable like above.

	const int COUNT = 22;

	double xPositions[COUNT] =
	{
		0.15, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55, 0.5
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3, 3.5
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i <= COUNT; i++)
	{
			delay += delays[i];

			if (i != 0)
			{
				position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

				BioEnemyShip* pEnemy = new BioEnemyShip();
				pEnemy->SetTexture(pTexture);
				pEnemy->SetCurrentLevel(this);
				pEnemy->Initialize(position, (float)delay);
				AddGameObject(pEnemy);
			}

				//Code created to draw & load in the power up.

			if (i == 0)
			{
					position.Set(xPositions[0] * Game::GetScreenWidth(), -pTextureTwo->GetCenter().Y);

					X3PowerUp* pPowerUp = new X3PowerUp(); // <- Power up object created.
					pPowerUp->SetTexture(pTextureTwo); // <-Texture loaded into variable.
					pPowerUp->SetCurrentLevel(this);
					pPowerUp->Initialize(position, (float)delay);
					AddGameObject(pPowerUp);

			}
	}


	Level::LoadContent(pResourceManager);
}